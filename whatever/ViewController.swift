//
//  ViewController.swift
//  whatever
//
//  Created by James Cash on 09-09-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layer.cornerRadius = 20
        self.view.layer.backgroundColor = UIColor.whiteColor().CGColor

        var sublayer = CALayer()
        sublayer.backgroundColor = UIColor.blueColor().CGColor
        sublayer.frame = CGRectMake(30, 30, 100, 100)
        sublayer.shadowOpacity = 0.8
        sublayer.shadowColor = UIColor.blackColor().CGColor
        sublayer.shadowRadius = 20
        sublayer.shadowOffset = CGSize(width: 20, height: 20)
        self.view.layer.addSublayer(sublayer)

        var fadeAnim = CABasicAnimation(keyPath: "opacity")
        fadeAnim.fromValue = 1.0
        fadeAnim.toValue = 0.0
        fadeAnim.duration = 1.5
        fadeAnim.autoreverses = true
//        fadeAnim.repeatCount = HUGE
//        sublayer.addAnimation(fadeAnim, forKey: "fading")

        var roundAnim = CABasicAnimation(keyPath: "cornerRadius")
        roundAnim.fromValue = 0.0
        roundAnim.toValue = 20.0
        roundAnim.duration = 1.5
        roundAnim.autoreverses = true

        var groupAnim = CAAnimationGroup()
        groupAnim.animations = [/*fadeAnim,*/ roundAnim]
        groupAnim.duration = 3.0
        groupAnim.repeatCount = HUGE
        sublayer.addAnimation(groupAnim, forKey: "fadingAndRound")

        var colorAnim = CAKeyframeAnimation(keyPath: "backgroundColor")
        colorAnim.values = [
            UIColor.blueColor().CGColor as CGColorRef,
            UIColor.greenColor().CGColor as CGColorRef,
            UIColor.redColor().CGColor as CGColorRef
        ]
        colorAnim.duration = 3.0
        colorAnim.repeatCount = HUGE
        sublayer.addAnimation(colorAnim, forKey: "technoparty")

        var shadowMoveAnim = CAKeyframeAnimation(keyPath: "shadowOffset.width")
        shadowMoveAnim.values = [10, 20, 10, -10, -20, -30]
        shadowMoveAnim.keyTimes = [0, 0.1, 0.2, 0.25, 0.7, 1]
        shadowMoveAnim.duration = 5
        shadowMoveAnim.repeatCount = HUGE
        sublayer.addAnimation(shadowMoveAnim, forKey: "shadowmove")


        var moveAnim = CAKeyframeAnimation(keyPath: "position")
        var layerPath = CGPathCreateMutable()
        CGPathMoveToPoint(layerPath, nil, 60, 60)
        CGPathAddLineToPoint(layerPath, nil, 0, 200)
        CGPathAddLineToPoint(layerPath, nil, 200, 150)
        CGPathAddLineToPoint(layerPath, nil, 164, 100)
        moveAnim.path = layerPath
        moveAnim.autoreverses = true
        moveAnim.duration = 5.0
        moveAnim.repeatCount = HUGE
        moveAnim.rotationMode = kCAAnimationRotateAuto
        //sublayer.addAnimation(moveAnim, forKey: "movingAround")

//        sublayer.speed = 3.0

    }

    override func viewDidLayoutSubviews() {
        self.view.layer.frame = CGRectInset(self.view.layer.frame, 20, 20)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

